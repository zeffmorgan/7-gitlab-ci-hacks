# CI/CD: Advanced workshop

Project used for webcast: https://about.gitlab.com/webcast/7cicd-hacks/

## Development

Have [docker installed](https://www.docker.com/products/docker-desktop) on your computer. Inside the root of this project run `docker-compose up`

